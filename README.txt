Description:
============
In this repository you have access of all benchmarking datasets used by iFeel, there is 19 datasets here.

A complete review of those datasets, how they were created and their referecens can be found at: "A Benchmark Comparison of State-of-the-Practice Sentiment Analysis Methods".


More information:
===============

@article{nunes2015benchmark,
  title={A Benchmark Comparison of State-of-the-Practice Sentiment Analysis Methods},
  author={Nunes Ribeiro, Filipe and Ara{\'u}jo, Matheus and Gon{\c{c}}alves, Pollyanna and Benevenuto, Fabr{\'\i}cio and Andr{\'e} Gon{\c{c}}alves, Marcos},
  journal={arXiv preprint arXiv:1512.01818},
  year={2015}
}

@inproceedings{gonccalves2013comparing,
  title={Comparing and combining sentiment analysis methods},
  author={Gon{\c{c}}alves, Pollyanna and Ara{\'u}jo, Matheus and Benevenuto, Fabr{\'\i}cio and Cha, Meeyoung},
  booktitle={Proceedings of the first ACM conference on Online social networks},
  pages={27--38},
  year={2013},
  organization={ACM}
}


@inproceedings{araujo2014ifeel,
  title={ifeel: A system that compares and combines sentiment analysis methods},
  author={Ara{\'u}jo, Matheus and Gon{\c{c}}alves, Pollyanna and Cha, Meeyoung and Benevenuto, Fabr{\'\i}cio},
  booktitle={Proceedings of the companion publication of the 23rd international conference on World wide web companion},
  pages={75--78},
  year={2014},
  organization={International World Wide Web Conferences Steering Committee}
}


Sorry to be brief,  send an email to matheus.araujo@dcc.ufmg.br if any questions.